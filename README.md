# Lottery-game

Simple lottery java game.


##Introduction

This game simulates a lottery. 
In the first step the program will draw 6 random numbers from range 1-49.
Then the lottery will take 6 numbers from player as input and search for the matches.
At the end of game, we will see computer numbers, player numbers and count of matches.


##Preview

![lottery game preview](lottery-game-preview.gif)


##Technologies

* Java


##Setup

To run this program, clone repository and compile files using your own IDE.

~~~
git clone https://glowkapracuje@bitbucket.org/glowkapracuje/lottery-game.git
~~~