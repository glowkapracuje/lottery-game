package pl.glowkapracuje;

import java.util.Arrays;

public class Main {

    private static final int RANGE = 49;
    private static final String HELLO_TEXT = "Hello! Let's start a game. Type six numbers from range: " + RANGE;


    public static void main(String[] args) {

        System.out.println(HELLO_TEXT);
        startTheGame(RANGE);
    }

    private static void startTheGame(int range) {

        PcNumbers pcNumbers = new PcNumbers();
        PlayerNumbers playerNumbers = new PlayerNumbers();
        Compare compare = new Compare();

        int[] pcNumbersArr = pcNumbers.getPcRandNums(range);
        int[] playerNumbersArr = playerNumbers.getPlayersNums(range);
        int countAppears = compare.countAppears(pcNumbersArr, playerNumbersArr);

        System.out.println("Pc numbers: " + Arrays.toString(pcNumbersArr));
        System.out.println("Your numbers: " + Arrays.toString(playerNumbersArr));

        compare.displayEndMssg(countAppears);

    }
}