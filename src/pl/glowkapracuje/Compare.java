package pl.glowkapracuje;

public class Compare {

    private static final String MSSG_1_MATCH = "You matched 1 number.";
    private static final String MSSG_2_MATCH = "You matched 2 numbers.";
    private static final String MSSG_3_MATCH = "It's nice, you matched 3 numbers.";
    private static final String MSSG_4_MATCH = "Good! You matched 4 numbers.";
    private static final String MSSG_5_MATCH = "Very good! You matched 5 numbers.";
    private static final String MSSG_6_MATCH = "Perfect! You matched 5 numbers.";
    private static final String MSSG_0_MATCH = "Hmm.. there's no any matches.. but you can try again :)";

    public Compare(){}

    /*
    DISPLAY MESSAGE AT THE END OF GAME
     */
    public void displayEndMssg (int countAppears){
        String endMessage;
        switch (countAppears){
            case 1:
                endMessage = MSSG_1_MATCH;
                break;
            case 2:
                endMessage = MSSG_2_MATCH;
                break;
            case 3:
                endMessage = MSSG_3_MATCH;
                break;
            case 4:
                endMessage = MSSG_4_MATCH;
                break;
            case 5:
                endMessage = MSSG_5_MATCH;
                break;
            case 6:
                endMessage = MSSG_6_MATCH;
                break;
            default:
                endMessage = MSSG_0_MATCH;
                break;
        }

        System.out.println(endMessage);
    }

    /*
    COMPARE 2 ARRAYS AND COUNT ALL MATCHES
     */
    public int countAppears(int[] pcArr, int[] playerArr) {

        int count = 0;

        for (int i = 0; i < pcArr.length; i++) {
            for (int j = 0; j < playerArr.length; j++) {
                if (pcArr[i] == playerArr[j]) {
                    count++;
                }
            }
        }

        return count;
    }
}
