package pl.glowkapracuje;

import java.util.Arrays;
import java.util.Random;

public class PcNumbers {

    public PcNumbers(){}

    /*
    GENERATE RANDOM UNIQUE PC NUMBERS AND RETURN SORTED ARRAY
     */
    public int[] getPcRandNums(int range) {

        Methods method = new Methods();

        int[] pcArr = new int[6];
        int randNum;

        for (int i = 0; i < pcArr.length; i++) {
            randNum = randomNumber(range);

            while (method.hasNumAppeared(pcArr, randNum)) {
                randNum = randomNumber(range);
            }

            pcArr[i] = randNum;
        }

        Arrays.sort(pcArr);
        return pcArr;

    }

    /*
    GENERATE RANDOM NUMBER FROM SPECIFIC RANGE
     */
    private static int randomNumber(int range) {
        Random random = new Random();
        return random.nextInt(range) + 1;
    }

}
