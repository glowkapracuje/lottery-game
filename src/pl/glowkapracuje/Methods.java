package pl.glowkapracuje;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Methods {

    private static final String ALERT_NO_NUMBER = "Use only digits!";
    private static final String ALERT_OUT_OF_RANGE = "Your number is out of range!";

    public Methods(){}

    /*
    SCANNING CORRECT NUMBER FROM CONSOLE
     */
    public int scanCorrectNumber(int range) {

        String mssgNoNumber = ALERT_NO_NUMBER;
        String mssgOutOfRange = ALERT_OUT_OF_RANGE;
        int scannedNum = 0;

        Scanner scanner = new Scanner(System.in);

        boolean numValidate = false;

        while (!numValidate) {
            try {
                scannedNum = scanner.nextInt();
                if (scannedNum > 0 && scannedNum <= range) {
                    numValidate = true;
                } else {
                    System.out.println(mssgOutOfRange);
                }
            } catch (InputMismatchException exception) {
                System.out.println(mssgNoNumber);
                scanner.nextLine();
            }
        }
        return scannedNum;
    }

    /*
    CHECK IF NUMBER APPEARED
     */
    public boolean hasNumAppeared(int[] arrToCheck, int numToCheck) {

        boolean found = false;

        for (int i = 0; i < arrToCheck.length; i++) {
            if (arrToCheck[i] == numToCheck) {
                found = true;
            }
        }

        return found;
    }

}