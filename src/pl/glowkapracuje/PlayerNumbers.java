package pl.glowkapracuje;

import java.util.Arrays;

public class PlayerNumbers {

    private static final String ALERT_NUM_APPEARED = "You type this number before, try with other one";

    private Methods methods;

    public PlayerNumbers(){}


    /*
    GET PLAYER NUMBERS FROM CONSOLE AND RETURN SORTED ARRAY
     */
    public int[] getPlayersNums(int range) {

        Methods methods = new Methods();

        int[] playerArr = new int[6];
        int scannedNum;

        for (int i = 0; i < playerArr.length; i++) {
            scannedNum = methods.scanCorrectNumber(range);

            while (methods.hasNumAppeared(playerArr, scannedNum)) {
                System.out.println(ALERT_NUM_APPEARED);
                scannedNum = methods.scanCorrectNumber(range);
            }
            System.out.println("ok");
            playerArr[i] = scannedNum;
        }

        Arrays.sort(playerArr);
        return playerArr;
    }
}
